from django.conf.urls import url
from django.urls import path

from . import views

app_name='accounts'

urlpatterns = [
    path('', views.log_page, name='log_page'),
    path('create_log', views.create_log, name='create_log'),
    path('log_list', views.log_list, name='log_list'),
]
