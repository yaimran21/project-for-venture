from django.contrib import admin
from django.contrib.admin import models
from django.forms import Textarea

from .models import EmployeeRecord


# Create admin model
class EmployeeRecordAdmin(admin.ModelAdmin):
    list_display = ('project', 'location')
    search_fields = ('project', 'location')
    list_filter = ('project', 'location', 'working_hours','user')


# Register my models here.
admin.site.register(EmployeeRecord,EmployeeRecordAdmin)
