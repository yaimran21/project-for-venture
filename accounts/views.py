from django.contrib import messages
from django.shortcuts import render, get_object_or_404
from .forms import EmplyeeRecordForm
from .models import EmployeeRecord


# Render the home page for employee to create and view  list log.
def log_page(request):
    return render(request, 'accounts/user_home.html')


# Create log for logged in user employee
def create_log(request):
    form = EmplyeeRecordForm(request.POST or None)
    if form.is_valid():
        instance = form.save(commit=False)
        instance.user = request.user
        instance.save()
    context = {
        'form': form
    }
    messages.success(request, 'Successfully added')

    return render(request, 'accounts/create_log.html', context)


# List of log
def log_list(request):
    instance = EmployeeRecord.objects.filter(user=request.user)
    context = {
        'instance': instance
    }
    messages.success(request, 'Successfully added')

    return render(request, 'accounts/log_list.html', context)


