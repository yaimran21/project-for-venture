from django.contrib.auth.models import User
from django.db import models


# Create a model for get every employee's all record
class EmployeeRecord(models.Model):
    project_name = (
        ('construction', 'Construction'),
        ('software-development', 'Software Development')
    )
    location = (
        ('inside', 'Inside Dhaka'),
        ('outside', 'Outside Dhaka')
    )

    user = models.ForeignKey(User, on_delete=models.CASCADE)
    project = models.CharField(max_length=100, choices=project_name)
    taskdate = models.DateTimeField(auto_now_add=True)
    location = models.CharField(max_length=100, choices=location)
    details = models.TextField(max_length=10000)
    working_hours = models.IntegerField(blank=True, null=True)
    active = models.BooleanField(default=True)
    note = models.TextField(max_length=200)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.project


