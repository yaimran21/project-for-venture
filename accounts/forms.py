from django import forms

from .models import EmployeeRecord


class EmplyeeRecordForm(forms.ModelForm):
    class Meta:
        model = EmployeeRecord
        fields = ['project', 'location', 'details', 'working_hours', 'active', 'note']
